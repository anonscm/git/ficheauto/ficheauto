#Script d'encodage des reponses du questionnaire Sphinx pour le mettre au format du fichier pepip
#@author Jean-Emmanuel Longueville
#@version 0.1
#MAJ 2017.04.26

############################ Definition de variable ############################
annee <- 2015
numversion <- "14" #sert � identifier les differents test.
seuilsalaire <- 10000

############################ Chargement des paquets ###############################
library(dplyr)
library(lubridate)
library(knitr)

############################ Definition des fonctions d'encodage ##################
source("fonctionsPepiCode.R")
source("MakePepiDico.R")

############################ Traitement des donnees ############################

#Avant de charger les donnees, copier le fichier de donnees dans le repertoire de travail et session -> setwd -> to source file location#
fic <- paste(getwd(),"DATA/2017-Rep.csv", sep="/") 
Sphinx_PEPIP <- read.csv2(fic, sep = ";", header=TRUE,  stringsAsFactors=FALSE)
Sphinx_PEPIP$apoine <- gsub(pattern = "\n", replacement = "", x = Sphinx_PEPIP$apoine ) #certain ine ont une fin de ligne en fin
dim(Sphinx_PEPIP)
#names(Sphinx_PEPIP)

source("MakePepiDico.R")
dim(Dico_PEPIP)

#Jointure des deux tables pour avoir un fichier complet
joinDicoSphinx <- full_join(Dico_PEPIP, Sphinx_PEPIP[,!names(Sphinx_PEPIP)%in%c("apoine","code_diplome")], by = c("CodeEtudiantD"="CodeEtudiant" ))
dim(joinDicoSphinx)

####debug annuel
joinDicoSphinx$q6_3 <- gsub(pattern="01/0201", replacement = "01/2001", x = joinDicoSphinx$q6_3 )
joinDicoSphinx$q6_3 <- gsub(pattern="03/0217", replacement = "03/2017", x = joinDicoSphinx$q6_3 )

Sortie <- joinDicoSphinx %>% filter(PROGRESSION != "1"| is.na(PROGRESSION)) %>% encodingfile()

#Selection des colonnes utiles
clnPepip <- c("num_etab","ine","code_diplome", "c_jour_nais", "c_mois_nais", "c_annee_nais", "c_annee_bac", "c_serie_bac", "c_sexe", "c_nation", "c_double_diplome", "c_a_enqueter", "statut_reponse", "date_reponse", "q2_1", "q2_2", "q2_3", "q2_4a", "q2_4_1", "q2_4_2", "q2_4_3", "q2_4_4", "q2_4_5", "q2_4_6", "q2_4_7", "q2_5", "q3_1_1", "q3_1_2", "q4_1", "q4_2s", "q4_2r","q4_3", "q5_1","q5_2","q5_3", "q5_4", "q6_1", "q6_2", "q6_3", "q6_4", "q6_5", "q6_6s", "q6_6r", "q6_7", "q6_8", "q6_9", "q6_10", "q6_11", "q6_12", "q6_13", "q6_14a", "q6_14b", "q6_15", "q6_16", "q6_17", "q7_1", "q8_1", "q8_2s", "q8_2r", "q8_3", "q8_4", "q8_5", "q8_6", "q8_7")


############################ Cr�ation des fichiers CSV et PDF ############################
if( length(clnPepip[which(!clnPepip %in% names(Sortie))])>0){#Debug en cas de colonne non definies selectionnees
  print("Les colonnes suivantes ne sont presentes que dans la variable des colonnes ministerielles :")
  print( clnPepip[which(!clnPepip %in% names(Sortie))] )
  stop("Le fichier Pepip ne peut pas �tre ecrit")
} else {
  pepip <- Sortie[,clnPepip]
  #names(pepip)[2]<-"ine"
  #Ecriture du fichier pour telechargement sur le site du ministere 
  write.table( pepip , file=paste(numversion,"-",year(Sys.time()),"_UPEC.csv",sep=""), sep = ";", col.names=TRUE, row.names=FALSE, fileEncoding = "latin1", na="", quote=FALSE)
  #write.table( fichierFinal , file=paste(numversion,"-",year(Sys.time()),"_UPECVerif.csv",sep=""), sep = ";", col.names=TRUE, row.names=FALSE, fileEncoding = "latin1", na="", quote=TRUE)
  knit2pdf("CalculTauxInsertion.Rnw", paste( numversion, annee+2, "TauxInsertion.tex", sep = "-"), quiet=TRUE)
}

#### Netoyage de variable ####
rm(joinDicoSphinx,Sphinx_PEPIP, Sortie)
cleanfiles()


############################ Analyse d'erreur ############################

#En cas de probl�me de modification des pays
Sortie$q6_14b <- unlist(sapply(Sortie$q6_14b,FUN=encodeCountry))
sort(unique(Sphinx_PEPIP$q6_14b))
sort(unique(Sortie$q6_14b))

#En cas de pb par rapport au ine pour voir ce qui bug
Sphinx_PEPIP[which(!(Sphinx_PEPIP$apoine%in%Dico_PEPIP$apoine)),"apoine"]
#Identification Doublon :
joinDicoSphinx[which(duplicated(joinDicoSphinx$INE, na.rm=TRUE)),"INE"]
Sphinx_PEPIP[which(duplicated(Sphinx_PEPIP$INE, na.rm=TRUE)),"INE"]

#Identification pb
etu="2404042136D" #INE
Sphinx_PEPIP[which(Sphinx_PEPIP$apoine==etu),]      #Fichier r�sultat sphinx
joinDicoSphinx[which(joinDicoSphinx$apoine==etu),]  # Fichier apr�s jointure
Sortie[which(Sortie$ine==etu),]                     # Df qui sera �crit pour remont�e Pepip
