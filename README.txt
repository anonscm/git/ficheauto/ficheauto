################################################################################
###                             MakeFiche                                    ###
################################################################################

1 Fichiers du programme
2 Fichiers source de donnees
3 Liens resultats

1 / Fichiers du programmes
-FicheDiplome.Rnw contient le code R et LaTeX permettant de creer une fiche diplome unique
-RunMakeFicheSimple.R Permet de lancer facilement avec peu de ligne de code l'execution des differentes
-MakeFicheR contient le code necessaire a l'etablissement des fiches
dipl�mes par dipl�mes et par composante
-Preambule contient l'ensemble des fonctions necessaires.

2 /Schema d'organisation des fichiers :
http://mocodo.wingi.net/
install.r : Gere l'installation des paquets necessaires.
preambule.R : Contient toutes les definitions de fonctions

Charge, 10 RunMakeFicheSimple.R, 01 preambule.R
RunMakeFicheSimple.R : lance Make fiche

Appelle, 11 RunMakeFicheSimple.R, 11 MakeFiche.R
MakeFiche.R :Decoupe et calcule les differentes fiches

Lance, N0 MakeFiche.R, 0N FicheDiplome.Rnw : Make fiche execute FicheDiplome N fois
FicheDiplome.Rnw : Structure d'une fiche fichier compile pour les obtenirs

Charge_1, 10 FicheDiplome.Rnw,  01 SourceAnnexe/Page1.R
Charge_2, 10 FicheDiplome.Rnw,  01 SourceAnnexe/Page2.R
Charge_3, 10 FicheDiplome.Rnw,  01 SourceAnnexe/Page3.R
Charge_4, 10 FicheDiplome.Rnw,  01 preamb.tex

SourceAnnexe/Page1.R : Code R de la page 1
SourceAnnexe/Page2.R : Code R de la page 2
SourceAnnexe/Page3.R : Code R de la page 3
preamb.tex : Preambule du fichier Tex issu du Rnw


2 Fichiers / Page source de donnees
Penser � supprimer les , et les ; avant export en csv, sinon ces characteres posent de nombreux pb.
Deux tableaux dont voici les colonnes les plus importantes (l'ordre de celles-ci n'est aucunement important):
Un contenant tous les inscrits  avec les colonnes suivantes :
  * CodeEtudiant        (Chiffre)
  * CodeEtapeApo        (AlphaNum)
  * LibEtapeApo         (AlphaNum)
  * dip                 (O/N)
  * Nationalite         (F/E)
  * TypedeBac           (AlphaNum)
  * RegimeInscription   (AlphaNum)
  * Sexe                (M/F)
  * DiplomeDomaine      (AlphaNum)
  * DiplomeMention      (AlphaNum)
  * ComposanteCode      (Chiffre)
  * ComposanteNom       (AlphaNum)
  * Boursier            (O/N)
  * anneedenaissance    (AlphaNum)
  * PrimoEntrant        (O/N)
  * Type_diplome        (AlphaNum)


#TODO reprendre cette partie pour correspondre au plus proche des fichiers
Un contenant tous les resultats de l'enquete
Colonne non contenues dans l'enquete et indispensable :
  * CodeEtudiant (Chiffre)
  * Genre
  * anneedenaissance
  * LibEtapeApo
  * q2_1
  * q2_2
  * q2_3
  * q2_4b
  * q2_4b AUTRE
  * q2_5
  * q3_1
  * q3_2_1
  * q3_2_2
  * q4_1
  * n1ContratTravail
  *n1TYPE CONTRAT
  * n1FORMATION
  * n1ORMATION_AUTRE
  * n1INTITULE
  * n1ETABLISSEMENT
  * n1RESULTAT
  * n2CONTRAT TRAVAIL1
  * n2TYPE CONTRAT1
  * n2FORMATION1
  * n2FORMATION_AUTRE1
  * n2INTITULE1
  * n2ETABLISSEMENT1
  * n2RESULTAT
  * n2CONTRAT TRAVAIL
  * n2TYPE CONTRAT
  * n2FORMATION
  * n2FORMATION_AUTRE
  * n2INTITULE
  * n2ETABLISSEMENT
  * q4_2
  * q4_3
  * q5_4
  * AU CHOMAGE DOMAINE DE RECHERCHE
  * q6_1
  * q6_2
  * q6_3
  * q6_4
  * EMPLOI AU 01122015 MOYEN
  * EMPLOI AU 01122015 MOYEN AUTRE
  * q6_5
  * q6_5 AUTRE
  * q6_6
  * q6_7
  * q6_8
  * q6_9
  * q6_10
  * q6_11
  * Remuneration
  * Contenu de votre emploi
  * Autonomie dans votre travail
  * Perspectives de carriere
  * Les missions qui vous sont confiees corr
  * Les enseignements que vous avez suivis e
  * q6_12
  * q6_12 AUTRE
  * EMPLOI AU 01122015 NOM EMPLOYEUR
  * q6_13
  * q6_14
  * q6_14b
  * Meme_emploi_2014-2015
  * Evolution_2014-2015
  * q6_15
  * q7_1
  * EMPLOI AVANT 2015
  * DATE RECHERCHE EMPLOI 2014
  * AU CHOMAGE DOMAINE DE RECHERCHE 2014
  * DATE DEBUT EMPLOI 2014
  * DATE FIN EMPLOI 2014
  * EMPLOI AU 01122014 MOYEN
  * EMPLOI AU 01122014 MOYEN_AUTRE
  * q8_1
  * q8_1 AUTRE
  * q8_2
  * q8_3
  * q8_4
  * q8_5
  * q8_6
  * q8_7
  * 1erEmploi_2014
  * q5_1
  * q6_16
  * q6_17
  * PREMIER EMPLOI 2 MOYEN
  * PREMIER EMPLOI 2 MOYEN AUTRE
  * PREMIER EMPLOI 2 STATUT
  * PREMIER EMPLOI 2 STATUT AUTRE
  * PREMIER EMPLOI 2 NIVEAU
  * PREMIER EMPLOI 2 TEMPS TRAVAIL
  * PREMIER EMPLOI 2 QUOTITE
  * PREMIER EMPLOI 2 SALAIRE
  * PREMIER EMPLOI 2 PRIMES OUI NON
  * PREMIER EMPLOI 2 PRIMES MONTANT
  * COMMENTAIRES
  * CodeEtudiant
  * Nationalite
  * NationaliteCodeAPO
  * NationaliteLibAPO
  * Sexe
  * TypedeBac
  * annee du bac
  * Bac detaille
  * MentionBacCode
  * MentionBacLib
  * Boursier
  * RegimeInscription
  * DiplomeCodeSise
  * DiplomeLibSise1
  * DiplomeLibSise2
  * CodeEtapeApo
  * DiplomeDomaine
  * DiplomeMention      (AlphaNum)
  * ComposanteApo
  * ComposanteCode
  * Pop MESR
  * Statut reponse
  * num_compos
  * num_etab
  * INE
  * code_diplome
  * libdip1
  * libdip2
  * libdip3
  * double_diplome
  * reinscri2
  * reinscri1
  * a_enqueter
  * c_jour_nais
  * c_mois_nais
  * c_annee_nais
  * c_annee_bac
  * c_serie_bac
  * c_sexe
  * c_nation
  * c_double_diplome
  * c_a_enqueter
  * DEGRE ACHEVEMENT SAISIE
  * STADE INTERRUPTION
  * REPONSE_TELEPHONE
  * PrimoEntrant

Ces deux tableaux peuvent etre sur deux feuilles d'un seul fichier excel (les inscrits en premiere page et les repondants en deuxieme) ou dans deux fichiers differents au format csv.
Ces deux possibilites ont ete developpees du fait que R dans le cas d'un grand nombre de ligne la gestion de fichier excel est tres mauvaise d'ou l'interet de passer par des fichiers csv.
