##### Partie poursuite d'�tude ####

##### Graphique sur la poursuite en fonction du domaine ####
tabl <- resc %>% select(DiplomeDomaine,q3_1) %>% table() %>% prop.table(1) %>% data.frame() %>% group_by(DiplomeDomaine) %>% mutate(pos = (0.5 * Freq)) #cumsum(Freq) - 

poursetudeDom <- ggplot(tabl, aes(x = factor(q3_1), y= Freq ,fill = factor(DiplomeDomaine) )) +
  geom_bar(position = "dodge", stat="identity") +
  theme_barre +
  theme(legend.position="bottom")+
  scale_fill_discrete( name=" ")+
  #guides(fill = guide_legend(nrow=4, byrow = TRUE, title=" ", order=0) )+
  labs(x=" ", y=" ")+
  scale_y_continuous(breaks=seq(0,1,0.1), labels=percent)+
  geom_text( mapping = aes( y = pos, label = lab(Freq) ), size=3, position = position_dodge(width =0.9))+
  guides(fill = guide_legend(nrow=3, byrow = FALSE, title=" ") )+
  theme(axis.text.x = element_text(size=5))

##### Graphique sur la nature des formations ####
#u3_2_1_3 , u3_2_2_3, u4_1_3
tabl <- gather(data = select(resc,u3_2_1_3 , u3_2_2_3, u4_1_3), key = "an",value = "Etude" ) %>% table() %>% prop.table(1) %>% data.frame() %>% mutate(
               an = gsub(pattern = "u3_2_1_3", replacement = paste(annee, annee+1, sep="/")   , x = an),
               an = gsub(pattern = "u3_2_2_3", replacement = paste(annee+1, annee+2, sep="/") , x = an),
               an = gsub(pattern = "u4_1_3",   replacement = paste(annee+2, annee+3, sep="/") , x = an),
               pos =(0.5 * Freq)) %>% group_by(an)

gpEtu <- ggplot(tabl, aes(x = factor(Etude), y = Freq, group=an, fill = factor(Etude) )) +
   facet_wrap(~an)+
   geom_bar(position = "dodge", stat="identity") +
   #coord_polar("y")+ #Passage en pie
   theme_barre + #fond blanc
   theme(legend.position="bottom", axis.text.x = element_blank())+
   scale_fill_discrete( name=" ")+
   guides(fill = guide_legend(nrow=2, byrow = FALSE, title=" ") )+
   labs(x="toto", y=" ")+xlab("")+
   geom_text( mapping = aes( y = pos, label = lab(Freq) ), position = position_dodge(width =0.7) , size=3)


