#### Definition de la fonction de boucle propre a MakeFiche ####
#' @title boucle
#' @author Jean-Emmanuel Longueville
#' @description This function is a diy for which make some control and split the data
#' @param typeForm Type of formatation : LP M2 MEEF
#' @param objBoucle Object use to split the data : "Etape / Composante / Mention / Domaine / Specialite"
#' @param colnameCode Column name from the data frame use to split the data often a code
#' @param colnameLib  Column name where the function search the label to present.
#' @warning colnameLib is required now @TODO this must be optionnal
#' @return 0 if all good 1 in other cases
#### 
boucle <- function(typeform, objBoucle, colNameCode, colNameLib){
  bug <- NA
  if( colNameCode %in% names(init_ins) & colNameCode %in% names(init_res)){
    levIns <- unique(init_ins[,colNameCode])   
    levRes <- unique(init_res[,colNameCode])
    nbdipins <- length(levIns)
    nbdipres <- length(levRes)
    typeForm <<- typeform 
    nbdipcommun <- length(intersect(levIns,levRes) )
    
    if (nbdipcommun != nbdipres){
      inter <- which( (levRes %in%  intersect(levIns,levRes)) == TRUE )
      bug <- 1
      msg <- paste("Repondant a des ", colNameLib, " differents \n", levRes[-inter], sep=" ")
    }else if(nbdipcommun != nbdipins){
      #Pour ce cas particulier on calcul quand m�me les fiches etant donne qu'il est possible qu'une categorie ai des inscrits mais aucun repondants.
      inter2 <-  which( (levIns %in%  intersect(levIns,levRes)) == TRUE )
      warnings(paste("/!\ ATTENTION Inscrits a des", colNameLib, "differents \n", levRes[-inter2], sep=" ") )
    }else{
      msg <- "Pas de soucis particulier"
    }
  }else if(!(colNameCode %in% names(init_ins))){
    bug <- 1
    msg <- paste("La colonne ", colNameCode, " est absente du fichier des inscrits") 
  }else if (!(colNameCode %in% names(init_res))){
    bug <- 1
    msg <- paste("La colonne ", colNameCode, " est absente du fichier des repondants")
  }
  
  if(is.na(bug)){
    i=1
    for(dipcode in intersect(levIns,levRes) ){
      diplabel <- init_res[which(init_res[, colNameCode]==dipcode), colNameLib][1]
      if(!is.na(diplabel)){
        diplabel <- gsub(pattern = "&", replacement = "et", x = diplabel)
        print(paste("On fait la fiche de ", diplabel, " (", dipcode, ") soit ", i, "/", nbdipcommun))
        #Warning /!\ Attention les libell�s contenu dans les colonnes LibEtapeApo doivent �tre strictement identiques sous peine de non consideration du dipl�me
        makefiche (dat_ins = subset(init_ins,init_ins[, colNameCode] == dipcode), dat_res = subset(init_res,init_res[, colNameCode] == dipcode), diplab=diplabel, dipcod=dipcode)
        i <- i+1
      }else{
        i<-i+1
      }
    }
  }else{
    warning(msg)
    sortie <-1
  }
  sortie <- 0
  return( sortie)
}
