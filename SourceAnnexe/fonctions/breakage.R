#### Definition des ensembles d'age pour le graphique age a l'obtention du dipl�me #### 
#' @title Break age
#' @author Jean-Emmanuel Longueville
#' @description This function set some value to split ages in cathegories
#' @seealso File : Page1.R Row : 82
#' @param age A vector with the age of repondants at time when they obtain their degree
#' @return a vector with the break
#' @example :
#' age <- trunc( rnorm(n = 200, mean = 24) )
#' breakage(age)
#' 

breakage <- function(age){
  br=c(23,24,25,29,39)
  if(min(age, na.rm=TRUE)<=br[1]){
    brek=min(age, na.rm=TRUE)-1
    i <- 1
  }else{
    i <- findInterval(min(age, na.rm=TRUE), br)
    brek <- br[i]
    i <- i+1
  }
  while( !is.na(br[i]) & br[i] < max(age, na.rm=TRUE)){
    brek <- c(brek, br[i])
    i <- i+1
  }
  brek <- c(brek,max(age, na.rm=TRUE)+1 )
  return(brek[!is.na(brek)])
}
