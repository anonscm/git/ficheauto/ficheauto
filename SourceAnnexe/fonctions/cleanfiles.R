#### Nettoyage des fichiers de compilation ####
#' @title Clean files
#' @author Jean-Emmanuel Longueville
#' @description This function remove all file create by the LaTeX compilation.
#' @seealso makefiche
#' @noparam
#' @example :
#' \code{cleanfiles()}
####

cleanfiles <- function(){
  lf <- list.files()
  regexp1 <- grep(".*[^preamb].*\\.tex$", lf)
  regexp2 <- grep(".*(log|aux|out|gz)$", lf)
  file.remove(lf[c(regexp1, regexp2)])
  unlink("figure", recursive = TRUE)
}
