#### Definition des listes de premier emploi sans doublon ####
#' @title Clean files
#' @author Jean-Emmanuel Longueville
#' @description This function return a list with : 
#' pe_n2 -> The job in n+2 is the first
#' pe_n1 -> The job in n+1 is the first
#' pe_pe -> The first job is not in dec n+1 or dec n+2
#' pe_liste -> group pe_n2, pe_n1, pe_pe
#' pe_no    -> No first job
#' pe_na    -> Fist job is indertermined
#' @return a list \seealso{\link{description}}
#' @param etuRep a dataframe with all aswers
#' @example :
#' \code{selpe(df)}
####
 
selpe <- function(etuRep){
  ##Determination vrai ou faux
  pen2 <- etuRep$u4_3 > 0 & ( etuRep$q6_15 == "Oui" & (etuRep$u6_17 != "Oui" | is.na(etuRep$u6_17) ) )
  pen1 <- etuRep$u4_3 > 0 & ( etuRep$u8_4  == "Oui" | (etuRep$q6_15 == "Oui" & etuRep$u6_17 == "Oui"))
  pen2[which(is.na(pen2))] <- FALSE
  pen1[which(is.na(pen1))] <- FALSE
  pena <- is.na(etuRep$u4_3) | (etuRep$u4_3 > 0 & (is.na(etuRep$q6_15)|etuRep$q6_15=="Non") & (is.na(etuRep$u8_4)|etuRep$u8_4=="Non") & (is.na(etuRep$q5_1)|etuRep$q5_1=="Non") & is.na(etuRep$q6_16)  ) #| (is.na(etuRep$q6_16)|etuRep$q5_1=="Oui")
  
  ##Selection des lignes  
  pe_no <- which(etuRep$u4_3 < 1 & !is.na(etuRep$u4_3)) #Pas d'emploi depuis le diplome
  pe_n2 <- which( pen2 ) #Premier emploi N+2 
  pe_n1 <- which( pen1 ) #Premier emploi N+1
  pe_na <- which( pena ) #Premier emploi NA (pas d'information)
  #TODO Simplifier pe_pe en �liminant !is.na(eturep$q6_16) quand inutile avec certitude
  pe_pe <- which( etuRep$u4_3 > 0 & (!is.na(etuRep$q6_16) | etuRep$q5_1 == "Oui" | is.na(etuRep$q6_15) | is.na(etuRep$u8_4))  & !(pen2 | pen1 | pena) )
  
  #Constitution de la liste des etudiants ayant eu un premier emploi
  pe_liste <- c(pe_n2,pe_n1,pe_pe)[order(c(pe_n2,pe_n1,pe_pe))]
  
  #Controle de coherence des listes
  if(length(intersect(pe_n2, pe_n1)) >0) {
    #pe_n2 <- pe_n2[-which(pe_n2 %in%pe_n1)]
    #	  print.xtable(xtable(), type ="latex", floating= FALSE, include.rownames=FALSE)
    print(etuRep[intersect(pe_n2, pe_n1),c("CodeEtudiant", "u4_3", "q4_3","q7_1","q5_1", "q6_15","u6_17", "u8_4", "q6_16", "q6_17","u10")]) #which(pe_n2 %in%pe_n1)
    stop("Les etudiants ci-dessus repondent aux conditions du premier emploi en n+2 et n+1 impossible de selectionner leur premier emploi")
  }
  
  if(length(intersect(pe_n2, pe_pe)) >0) {
    #pe_pe <- pe_pe[-which(pe_pe %in%pe_n2)]
    print(etuRep[intersect(pe_n2, pe_pe),c("CodeEtudiant", "u4_3", "q4_3","q7_1","q5_1", "q6_15","u6_17", "u8_4", "q6_16", "q6_17","u10")]) #which(pe_pe %in%pe_n2)
    stop("Les etudiants ci-dessus repondent aux conditions du premier emploi en n+2 et du premier emploi general impossible de selectionner leur premier emploi")
  }
  if(length(intersect(pe_n1, pe_pe)) >0) {
    #pe_pe <- pe_pe[-which(pe_pe %in%pe_n1)]
    print(etuRep[intersect(pe_n1, pe_pe),c("CodeEtudiant", "u4_3", "q4_3","q7_1","q5_1", "q6_15","u6_17", "u8_4", "q6_16", "q6_17","u10")]) #which(pe_pe %in%pe_n1)
    stop("Les etudiants ci-dessus repondent aux conditions du premier emploi en n+1 et du premier emploi general impossible de selectionner leur premier emploi")
  }
  if(length(intersect(pe_liste, pe_na)) >0) {
    #pe_pe <- pe_pe[-which(pe_pe %in%pe_n1)]
    print(etuRep[intersect(pe_liste, pe_na),c("CodeEtudiant", "u4_3", "q4_3","q7_1","q5_1", "q6_15","u6_17", "u8_4", "q6_16", "q6_17","u10")]) #which(pe_pe %in%pe_n1)
    stop("Les etudiants ci-dessus repondent aux conditions du premier emploi et aucun emploi impossible de selectionner leur premier emploi")
  }
  
  #pe_pb <- etuRep[-c(pe_n2,pe_n1,pe_pe,pe_no,pe_na)[order(c(pe_n2,pe_n1,pe_pe,pe_no,pe_na))],c("u4_3", "q5_1","q7_1","q4_3", "q6_15", "u8_4", "q6_16")] #Attention le vecteur suivant le - dans la selection de ligne doit être ordonne pour être correct
  #pe_pbDate <- etuRep[-c(pe_n2,pe_n1,pe_pe,pe_no,pe_na)[order(c(pe_n2,pe_n1,pe_pe,pe_no,pe_na))],c("u4_3", "q6_3","u8_1", "q6_16")]
  #pe_pbPE <- etuRep[-c(pe_n2,pe_n1,pe_pe,pe_no,pe_na)[order(c(pe_n2,pe_n1,pe_pe,pe_no,pe_na))],c("u4_3", "q6_15","u8_4", "q5_1")]
  #rm(pe,pe_na,pe_pe,pen1,pen2,pe_no,pe_n1,pe_n2,pe_liste,pe_pbPE)
  #Verification
  if( (length(pe_liste)+length(pe_no)+length(pe_na) ) != nrow(etuRep) ){
    cat("Oo Patron t'a un cheveu dans la soupe oO\n")
    cat("   \\\\ \n    \\\\_    \\\\\n    (')     \\\\_\n    / )=.---(')\n  o( )o( )_-\\_\n")
    
    cat(paste("\n  Nombre de personne en emploi : ", length(pe_liste) ,"\n"))
    cat(paste("Nombre de personne sans emploi : ", length(pe_no) ,"\n"))
    cat(paste("  Nombre de personne en erreur : ", length(pe_na), "\n"))
    cat(      "                                __________________","\n")
    cat(paste("                         Sous total : ", length(pe_liste)+length(pe_no)+length(pe_na) ,"\n"))
    cat(paste("           Nombre de personne total : ", nrow(etuRep) ,"\n"))
    cat(paste("Nombre de difference total - trouve : ", nrow(etuRep)-(length(pe_liste)+length(pe_no)+length(pe_na)) ,"\n"))
    return(etuRep[-c(pe_n2,pe_n1,pe_pe,pe_no,pe_na)[order(c(pe_n2,pe_n1,pe_pe,pe_no,pe_na))],c("u4_3", "q6_15","u8_4", "q5_1")])
  }else{
    cat(paste("Tout va bien\n On observe bien :",nrow(etuRep),"\n"))
    return(list(pe_n2=pe_n2, pe_n1=pe_n1, pe_pe=pe_pe,pe_liste=pe_liste, pe_no=pe_no, pe_na=pe_na ))
  }
}
