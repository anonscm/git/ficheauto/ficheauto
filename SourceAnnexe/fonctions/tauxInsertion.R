#### Calcul du taux d'insertion ####
#' @title Taux d'Insertion
#' @author Jean-Emmanuel Longueville
#' @description This function compute the indice : insertion rate
#' @param vec a vector with Emploi Chomage or Inactif value
#' @param weight an optionnal vector with weight for redressement
#' @return the value of the indice
#' @examples 
#' \code{
#' test <- c("Emploi","Emploi","Emploi", "Chomage","Chomage","Chomage")
#' tauxInsertion(test)
#' }
##### 
tauxInsertion <- function(vec, weight){
  if(!exists("weight")){
    emploi <- length(which(vec=="Emploi"))
    chomage <- length(which(vec=="Chomage"))
    return(round(emploi/(emploi+chomage),digits=2) )
  }else{
    dat  <- cbind.data.frame(weight,vec)
    tabl <- dat %>% count(vec, wt= weight) %>% as.data.frame()
    chomage <- ifelse(length(which(tabl$vec=="Chomage"))>0, tabl[which(tabl$vec=="Chomage"),"n"], 0)
    emploi  <- ifelse(length(which(tabl$vec=="Emploi"))>0, tabl[which(tabl$vec=="Emploi" ),"n"], 0)
    return(round(emploi/(emploi+chomage),digits=2) )
  }
}

