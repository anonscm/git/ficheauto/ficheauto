#### Calcul du taux de poursuite d'�tude ####
#' @title Taux d'Insertion
#' @author Jean-Emmanuel Longueville
#' @description This function compute the indice : studies rate
#' @param vec a vector with Emploi Chomage or Inactif value or a dataframe with 1 column
#' @param weight a vector of ponderation values 
#'
#' @return the value of the indice
#' @examples 
#' \code{
#' test <- c("Oui","Non","Non", "Non","Non","Oui")
#' tauxPEtude(test)
#' }
##### 
tauxPEtude <- function(vec, weight){
  if(!exists("weight")){
    if( class(vec) == "data.frame"){ vec = vec[,1]}
    Etude <- length(which(vec=="Oui"))
    Total <- length(vec)
    return(round(Etude/Total,digits=2) )
  }else{
    dat  <- cbind.data.frame(weight,vec)
    pat  <- svydesign(ids=~1, weights = ~weight, data = dat)
    tabl <- svytable( ~vec, pat , round=TRUE)
    return(round(tabl[2]/sum(tabl),digits=2))
  }
}
