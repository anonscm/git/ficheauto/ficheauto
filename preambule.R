#Fichier de preambule de R definition des differentes fonctions et de differentes variables necessitant des evolutions avec l'evolution des questionnaires.
#v0.9.1-2018-06-21
#'@author Jean-Emmanuel Longueville

if(length(grep("SourceAnnexe", list.dirs())) == 0 ){
  wd <- getwd()
  setwd(dir = normalizePath("../")) #On remonte d'un niveau dans l'arborescence.
  flag <- TRUE
}  ## On est au bon endroit

############################ Chargement des paquets ###############################
#       (o_
#(o_    //\		
#(/)_   V_/_

library(knitr)		#Permet la transformation du .Rnw en .Tex -> Pdf
library(ggplot2)	#Graphique
library(lubridate)	#Gestion des dates
library(scales)		#Gestion des echelles (axes) permet l'usage des pourcentage sur les axes
library(cowplot)	#Gestion plus fine des representation graphique
#options(java.parameters = "-Xmx4000m")
library(xlsx)		#Assure la lecture des fichiers xlsx
library(dplyr)
library(xtable)#Pour permettre l'affichage de tableau R dans LaTeX


############################ Definition des variables globales souvent reprise ###############################
#       (o_
#(o_    //\			
#(/)_   V_/_
jobvar  <- "Emploi"
chomm  <- "Chomage" 
inactif <- "Inactif"
jobvarpasse  <- "Emploi"
chommpasse  <- "Chomage"
inactifpasse <- "Inactif"

############################ Theme pour GGplot reutilise par la suite  ###############################
#       (o_
#(o_    //\			
#(/)_   V_/_

theme_pie <- theme_minimal() +
  theme(
	axis.title.x = element_blank(),
	axis.text.x = element_blank(),
	axis.title.y = element_blank(),
	panel.border = element_blank(),
	panel.grid = element_blank(),
	axis.ticks = element_blank(),
	plot.title = element_text(colour = 'red', size = 8, face = "bold", hjust = 0.5, vjust = 0.5),
	legend.text = element_text(size = 7),
	legend.key.size = unit(2, "mm"),
	panel.background = element_rect(fill = "transparent",colour = NA), # or theme_blank()
  plot.background  = element_rect(fill = "transparent",colour = NA),
  panel.grid.minor = element_blank(), 
  panel.grid.major = element_blank()
	) 

theme_barre <- theme_bw() +
  theme(
	panel.grid = element_blank(),
	plot.title = element_text(colour = 'red', size = 8, face = "bold", hjust = 0.5, vjust = 0.5),
	legend.text = element_text(size = 9),
	legend.key.size = unit(2, "mm"),
	panel.background = element_rect(fill = "transparent",colour = NA), # or theme_blank()
  plot.background  = element_rect(fill = "transparent",colour = NA),
  panel.grid.minor = element_blank(),
	plot.margin = unit(c(1,0,0,0), units = "mm")
	)
theme_vide <- theme_minimal() +
  theme(
    plot.title = element_text(colour = 'red', size = 8, face = "bold", hjust = 0.5, vjust = 0.5),
    axis.title.x = element_blank(), 
    axis.text.x = element_blank(), 
    axis.ticks.x = element_blank(), 
    axis.title.y = element_blank(), 
    axis.text.y = element_blank(), 
    axis.ticks.y = element_blank()
    )

textenb <- geom_text(
		mapping = aes(  y = (..count.. / 2), label = paste0(..count.., "") ), 
		stat = "count", 
		position = position_dodge(width = 0.7),
		na.rm = TRUE,
		size = 3
)

graphvide <-  ggplot() + annotate("text", x =  5, y = 5, label = "Donn�es insuffisantes") +  
  theme_vide

############################ Chargements des fonctions ############################
#          __     __
#         /  \~~~/  \
#   ,----(     ..    )
#  /      \__     __/
# /|         (\  |(
#^ \   /___\  /\ |   hjw
#   |__|   |__|-"
#on peut utiliser normalizepath() pour
source("SourceAnnexe/fonctions/lancecalcul.R")
source("SourceAnnexe/fonctions/ChargeTableau.R")
source("SourceAnnexe/fonctions/adaptionData.R")
source("SourceAnnexe/fonctions/defvar.R")
source("SourceAnnexe/fonctions/lab.R")
source("SourceAnnexe/fonctions/selpe.R")
source("SourceAnnexe/fonctions/getaccesstime.R")
source("SourceAnnexe/fonctions/getnbjob.R")
source("SourceAnnexe/fonctions/lib.R")
source("SourceAnnexe/fonctions/breakage.R")
source("SourceAnnexe/fonctions/makefiche.R")
source("SourceAnnexe/fonctions/boucle.R")
source("SourceAnnexe/fonctions/geo.R")
source("SourceAnnexe/fonctions/moyenacces.R")
source("SourceAnnexe/fonctions/rangepdf.R")
source("SourceAnnexe/fonctions/ReducProf.R")
source("SourceAnnexe/fonctions/ReducNiv.R")
source("SourceAnnexe/fonctions/ReducTypPatron.R")
source("SourceAnnexe/fonctions/str_sentence_case.R")
source("SourceAnnexe/fonctions/cleanfiles.R")
source("SourceAnnexe/fonctions/mv.R")
source("SourceAnnexe/fonctions/def_a_enqueter.R")


###Function pour docWord ####
source("SourceAnnexe/fonctions/addGraph.R")
source("SourceAnnexe/fonctions/tableTxrep.R")
source("SourceAnnexe/fonctions/tauxInsertion.R")
source("SourceAnnexe/fonctions/tauxPEtude.R")
source("SourceAnnexe/fonctions/tauxEmpStable.R")
source("SourceAnnexe/fonctions/tauxCadre.R")
source("SourceAnnexe/fonctions/tauxTp.R")

if( exists("flag") ){
  setwd(wd) #On remonte d'un niveau dans l'arborescence.
  rm(wd, flag)
}  ## On est au bon endroit
